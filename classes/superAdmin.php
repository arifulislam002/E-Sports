<?php

//session_start();
class SuperAdmin {
    protected $dbConnect;
 


    public function __construct() {
        $hostName='localhost';
        $userName='root';
        $password='';
        $dbName='db_eshopper';
        
        $this->dbConnect=  mysqli_connect($hostName, $userName, $password, $dbName);
        if(!$this->dbConnect){
            
            die("Connection Failed".mysqli_error($this->dbConnect));
            
        }  
         
     }
     public function saveCategory($data){

         
         $sql="insert into tbl_category (category_name,category_description,publication_status) values ('$data[categoryName]','$data[categoryDescription]','$data[publicationStatus]')";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             $message="Category inserted successfully !!!";
             return $message;
         }
        else {
             die("Connectin Error".mysqli_error($this->dbConnect));
        }
     }
     public function showCategoryInfo(){
         
         $sql="select * from tbl_category";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
     }
     public function unpublishCategory($categoryId) {
         $sql="update tbl_category set publication_status = 0 where category_id='$categoryId'";
         $query=  mysqli_query($this->dbConnect, $sql);
         if($query){
             $message="Unpublished !!!";
             return $message;
         }
         
     }
      public function publishCategory($categoryId) {
         $sql="update tbl_category set publication_status = 1 where category_id='$categoryId'";
         $query=  mysqli_query($this->dbConnect, $sql);
         if($query){
             $message="Published !!!";
             return $message;
         }
         
     }
     public function showCategoryInfoById($categoryId){
         
         $sql="select * from tbl_category where category_id='$categoryId'";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
         
     }
     public function editCategoryById($dataUpdate){
         
         $sql="update tbl_category set category_name='$dataUpdate[categoryName]',category_description='$dataUpdate[categoryDescription]',publication_status='$dataUpdate[publicationStatus]' where category_id=$dataUpdate[categoryId]";
         $query=mysqli_query($this->dbConnect,$sql);
         
         if ($query){
             $_SESSION['message']="Update Successful !!!";
             header('Location:manageCategory.php');
         }
     }
     public function deleteCategory($categoryId){
         $sql="delete from tbl_category where category_id='$categoryId'";
         $query=  mysqli_query($this->dbConnect, $sql);
         if($query){
             $message="Deleted successfully !!!";
             return $message;
         }
     }
     public function saveManufacturer($data){

         
         $sql="insert into tbl_manufacturer (manufacturer_name,manufacturer_description,publication_status) values ('$data[manufacturerName]','$data[manufacturerDescription]','$data[publicationStatus]')";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             $message="Manufacturer inserted successfully !!!";
             return $message;
         }
        else {
             die("Connectin Error".mysqli_error($this->dbConnect));
        }
     }
     public function showManufacturerInfo(){
         
         $sql="select * from tbl_manufacturer";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
     }
     public function unpublishManufacturer($manufacturerId) {
         $sql="update tbl_manufacturer set publication_status = 0 where manufacturer_id='$manufacturerId'";
         $query=  mysqli_query($this->dbConnect, $sql);
         if($query){
             $message="Unpublished !!!";
             return $message;
         }
         
     }
      public function publishManufacturer($manufacturerId) {
         $sql="update tbl_manufacturer set publication_status = 1 where manufacturer_id='$manufacturerId'";
         $query=  mysqli_query($this->dbConnect, $sql);
         if($query){
             $message="Published !!!";
             return $message;
         }
         
     }
     public function showManufacturerInfoById($manufacturerId){
         
         $sql="select * from tbl_manufacturer where manufacturer_id='$manufacturerId'";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
         
     }
     public function editManufacturerById($dataUpdate){
         
         $sql="update tbl_manufacturer set manufacturer_name='$dataUpdate[manufacturerName]',manufacturer_description='$dataUpdate[manufacturerDescription]',publication_status='$dataUpdate[publicationStatus]' where manufacturer_id=$dataUpdate[manufacturerId]";
         $query=mysqli_query($this->dbConnect,$sql);
         
         if ($query){
             $_SESSION['message']="Update Successful !!!";
             header('Location:manageManufacturer.php');
         }
     }
     public function deleteManufacturer($manufacturerId){
         $sql="delete from tbl_manufacturer where manufacturer_id='$manufacturerId'";
         $query=  mysqli_query($this->dbConnect, $sql);
         if($query){
             $message="Deleted successfully !!!";
             return $message;
         }
     }
     public function saveProduct($data){
         
         $directory='../assets/frontEndAssets/productImages/';
         $targetFile=$directory.$_FILES['productImage']['name'];
         $fileType= pathinfo($targetFile,PATHINFO_EXTENSION);
         $fileSize=$_FILES['productImage']['size'];
         $image=  getimagesize($_FILES['productImage']['tmp_name']);
         
         if ($image){
             if (file_exists($targetFile)){
                 $msg="File already exists....!!!";
                 return $msg;
                 exit();
             }  else {
                 if ($fileSize > 5242880){
                     $msg="File must be with in 5MB";
                     return $msg;
                 }  else {
                     if ($fileType!='jpg'and $fileType!='png'){
                         $msg="Please insert .jpg or .png file";
                         return $msg;
                     }
                    else {
                        
                        move_uploaded_file($_FILES['productImage']['tmp_name'],$targetFile);
                        $sql = "insert into tbl_product(product_name,category_id,manufacturer_id,product_price,product_quantity,product_sku,product_image,product_description,publication_status) values ('$data[productName]','$data[categoryId]','$data[manufacturerId]','$data[productPrice]','$data[productQuantity]','$data[productSKU]','$targetFile','$data[productDescription]','$data[publicationStatus]')";
                        $query = mysqli_query($this->dbConnect, $sql);
                        if ($query) {

                            $msg = "Product inserted successfully";
                            return $msg;
                        } else {
                            die("Query Problem").mysqli_error($this->dbConnect);
                        }
                    }
                     
                 }
             }
         }  else {
            $msg="Please insert a valid image";
            return $msg;
         }
         
         

     }
     public function showProductInfo(){
         
         $sql="select * from tbl_product";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
     }
     public function unpublishProduct($productId) {
         $sql="update tbl_product set publication_status = 0 where product_id='$productId'";
         $query=  mysqli_query($this->dbConnect, $sql);
         if($query){
             $message="Unpublished !!!";
             return $message;
         }
         
     }
      public function publishProduct($productId) {
         $sql="update tbl_product set publication_status = 1 where product_id='$productId'";
         $query=  mysqli_query($this->dbConnect, $sql);
         if($query){
             $message="Published !!!";
             return $message;
         }
         
     }
     public function showProductInfoById($productId){
         
         $sql="select * from tbl_product where product_id=$productId";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
         
     }
    
     

     public function logOut(){
         
         unset($_SESSION['adminId']);
         unset($_SESSION['adminName']);
         header('Location:index.php');
     }
}
