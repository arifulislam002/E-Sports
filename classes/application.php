<?php


class Application {
    
    protected $dbConnect;
    
     public function __construct() {
        $hostName='localhost';
        $userName='root';
        $password='';
        $dbName='db_eshopper';
        
        $this->dbConnect=  mysqli_connect($hostName, $userName, $password, $dbName);
        if(!$this->dbConnect){
            
            die("Connection Failed".mysqli_error($this->dbConnect));
            
        }  
         
     }
     public function showCategory(){
         
         $sql="select * from tbl_category where publication_status=1";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
     }
     public function showManufacturers(){
         
         $sql="select * from tbl_manufacturer where publication_status=1";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
     }
     public function showPublishedProduct() {
         
         $sql="select * from tbl_product where publication_status=1 group by category_id";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
         
     }
     public function showLimitedPublishedProduct() {
         
         $sql="select * from tbl_product where publication_status=1 group by category_id";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
         
     }
     public function showProductByCategory($categoryId) {
         
         $sql="select * from tbl_product where publication_status=1 and category_id=$categoryId";
         //$sql2="select c.category_name,m.manufacturer_name,p.product_name from tbl_category c,tbl_manufacturer m , tbl_product p where c.category_id=p.category_id and m.manufacturer_id=p.manufacturer_id";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
         
     }
     public function showProductDetailById($productId) {
         
         $sql="select p.product_id,p.product_name,c.category_name,m.manufacturer_name,p.product_price,p.product_quantity,p.product_image from tbl_category c,tbl_manufacturer m , tbl_product p where c.category_id=p.category_id and m.manufacturer_id=p.manufacturer_id and p.product_id=$productId";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
         
     }
      public function addProductToTempCart($data) {
         $productId=$data['productId'];
         $sql="select * from tbl_product where product_id=$productId";
         $query=  mysqli_query($this->dbConnect, $sql);
         $productInfo=  mysqli_fetch_assoc($query);
         session_start();
         $sessionId =  session_id();
         
         $sqlInsert="insert into tbl_temp_cart(session_id,product_id,product_name,product_price,purchase_quantity,product_image) values('$sessionId','$productId','$productInfo[product_name]','$productInfo[product_price]','$data[purchaseQuantity]','$productInfo[product_image]')";
         $queryInsert=  mysqli_query($this->dbConnect, $sqlInsert);
         if ($queryInsert){
         header('Location:cart.php');
         }
     }
     public function showCartInfoBySessionId($sessionId) {
         
         $sql="select * from tbl_temp_cart where session_id='$sessionId'";
         $query=  mysqli_query($this->dbConnect, $sql);
         if ($query){
             return $query;
         }
         
     }
     public function deleteCartItemById($tempCartId){
         $sql="delete from tbl_temp_cart where temp_cart_id=$tempCartId";
         $query=  mysqli_query($this->dbConnect, $sql);
         if($query){
             $msg='Your cart item deleted succesfully !!!';
             return $msg;
         }
     }
    
}
