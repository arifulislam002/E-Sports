<?php 
ob_start();
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-SPORTS</title>
    <link href="assets/frontEndAssets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/frontEndAssets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/frontEndAssets/css/prettyPhoto.css" rel="stylesheet">
    <link href="assets/frontEndAssets/css/price-range.css" rel="stylesheet">
    <link href="assets/frontEndAssets/css/animate.css" rel="stylesheet">
    <link href="assets/frontEndAssets/css/main.css" rel="stylesheet">
    <link href="assets/frontEndAssets/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="assets/frontEndAssets/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/frontEndAssets/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/frontEndAssets/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/frontEndAssets/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/frontEndAssets/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
            <?php include './includes/headerTop.php';?>
            <?php include './includes/headerMiddle.php';?>
            <?php include './includes/headerBottom.php';?>

		<!--/header-bottom-->
	</header><!--/header-->
	<?php
        if (isset($pages)){
            if ($pages=='contactUs')include './pages/contactUsContent.php';
                
            elseif ($pages=='productContent') include './pages/productContent.php';
            elseif ($pages=='productDetailContent') include './pages/productDetailContent.php';
            elseif ($pages=='cartContent') include './pages/cartContent.php';
                
            
        }
        else    include './pages/homeContent.php';
        ?>
	<!--home_content -->
	
	<footer id="footer"><!--Footer-->
		<!--footer_top-->
                <?php include './includes/footerTop.php';?>
		
		<!--footer_widget-->
                <?php include './includes/footerWidget.php';?>
		
		<!--footer_Bottom-->
                <?php include './includes/footerBottom.php';?>
		
	</footer><!--/Footer-->
	

  
        <script src="assets/frontEndAssets/js/jquery.js"></script>
        <script src="assets/frontEndAssets/js/bootstrap.min.js"></script>
        <script src="assets/frontEndAssets/js/jquery.scrollUp.min.js"></script>
        <script src="assets/frontEndAssets/js/price-range.js"></script>
        <script src="assets/frontEndAssets/js/jquery.prettyPhoto.js"></script>
        <script src="assets/frontEndAssets/js/main.js"></script>
</body>
</html> 