<?php
$sessionId=  session_id();
$queryResult=$objApplication->showCartInfoBySessionId($sessionId);

if (isset($_GET['status'])){
    if ($_GET['status']=='delete'){
        $tempCartId=$_GET['tempCartId'];
        $msg=$objApplication->deleteCartItemById($tempCartId);
    }
}
?>

<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Shopping Cart</li>
            </ol>
            <h2 style="color: green"><?php if (isset($msg)){echo $msg;$queryResult=$objApplication->showCartInfoBySessionId($sessionId);unset($msg);} ?></h2>
        </div>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description"></td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <?php $sum=0; while ($cartInfo=  mysqli_fetch_assoc($queryResult)) {?>
                    <tr>
                        <td class="cart_product">
                            <a href=""><img src="pages/<?php echo $cartInfo['product_image'] ?>" alt="" width="50px" height="50px"></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href=""><?php echo $cartInfo['product_name'] ?></a></h4>
                            <p>Product ID: <?php echo $cartInfo['product_id'] ?></p>
                        </td>
                        <td class="cart_price">
                            <p><?php echo $cartInfo['product_price'] ?></p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <!--<form name="updateQuantity"action="" method="post">-->
                                <input type="submit" class="cart_quantity_up" value="+" onclick=" addQuantity()">
                                <input class="cart_quantity_input" type="text" name="quantity" value="<?php echo $cartInfo['purchase_quantity'];?>" autocomplete="off" size="2">
                                <h4 id="showQ"></h4>
                                <input type="submit" class="cart_quantity_down" href="" value="-"onclick="return subQuantity()">
                                <!--</form>-->
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price"><?php echo $price=($cartInfo['product_price']*$cartInfo['purchase_quantity']);$sum=$sum+$price; ?></p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href="?status=delete&&tempCartId=<?php echo $cartInfo['temp_cart_id'] ?>"><i class="fa fa-times" title="Delete"></i></a>
                        </td>
                    </tr>
                    <?php } ?>

               
               
                </tbody>
            </table>
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Sub Total</td>
                        <td class="price">Vat (15%)</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                     
                       <td class="cart_total">
                            <p class="cart_total_price">BDT : <?php echo $sum ?></p>
                        </td>
                         <td class="cart_total">
                            <p class="cart_total_price">BDT : <?php echo $vat=$sum*.15 ?></p>
                        </td>
                         <td class="cart_total">
                            <p class="cart_total_price">BDT : <?php echo $total=$sum+$vat ?></p>
                        </td>
                    </tr>
                   

               
               
                </tbody>
            </table>
        </div>
    </div>
</section>
<script>

 function addQuantity(){
     <?php echo $cartInfo=mysqli_fetch_assoc($queryResult)?> ;
     var quantity=Number("<?php echo $cartInfo['purchase_quantity']?> ");
     quantity=quantity+3;
     //document.getElementByName('quantity').value=quantity;
     document.getElementById('showQ').innerHTML=quantity;
   
    
     
                                   
 }
</script>
