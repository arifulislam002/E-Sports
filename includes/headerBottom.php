<?php 
require './classes/application.php';
$objApplication=new Application();
$queryResult=$objApplication->showCategory();

?>
<div class="header-bottom"><!--header-bottom-->
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="mainmenu pull-left">
                    <ul class="nav navbar-nav collapse navbar-collapse">
                        <li><a href="index.php" class="">Home</a></li>
                        <?php while ($showCategory=  mysqli_fetch_assoc($queryResult)){ ?>
                        <li><a href="product.php?categoryId=<?php echo $showCategory['category_id']; ?>"><?php echo $showCategory['category_name']; ?></a></li>
                        <?php }?>
                        <li><a href="contactUs.php" class="">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="search_box pull-right">
                    <input type="text" placeholder="Search"/>
                </div>
            </div>
        </div>
    </div>
</div>

