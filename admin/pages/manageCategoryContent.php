<?php
 $queryResult=$objSuperAdmin->showCategoryInfo();
 $categoryId=$_GET['categoryId'];

if(isset($_GET['publicationStatus'])){
   
    if($_GET['publicationStatus']=='published'){
        
        $catMsgU=$objSuperAdmin->unpublishCategory($categoryId);
    }
    elseif ($_GET['publicationStatus']=='unPublished') {
        
        $catMsgP=$objSuperAdmin->publishCategory($categoryId);
    
    }
    elseif ($_GET['publicationStatus']=='delete') {
        
        $delMsg=$objSuperAdmin->deleteCategory($categoryId);
    
    }
}
?>
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="index.html">Home</a> 
        <i class="icon-angle-right"></i>
    </li>
    <li><a href="#">Category at a glance</a></li>
</ul>
<h2 style="color:  red ">
<?php 
if (isset($catMsgU)){
        echo $catMsgU;
        $queryResult=$objSuperAdmin->showCategoryInfo();
}
        unset($catMsgU);
    ?>
</h2>
<h2 style="color: green ">
<?php 
if (isset($catMsgP)){
        echo $catMsgP;
        $queryResult=$objSuperAdmin->showCategoryInfo();
        }
        unset($catMsgP);
    ?>
</h2>
<h2 style="color: green ">
<?php 
if (isset($_SESSION['message'])){
        echo $_SESSION['message'];
        $queryResult=$objSuperAdmin->showCategoryInfo();
        }
        unset($_SESSION['message']);
    ?>
</h2>
<h2 style="color:  red ">
<?php 
if (isset($delMsg)){
        echo $delMsg;
        $queryResult=$objSuperAdmin->showCategoryInfo();
}
        unset($delMsg);
    ?>
</h2>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Categories </h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Category ID</th>
                        <th>Category Name</th>
                        <th>Description</th>
                        <th>Publication Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php while ($categoryInfo=  mysqli_fetch_assoc($queryResult)){ ?>
                    <tr>
                        <td><?php echo $categoryInfo['category_id']?></td>
                        <td class="center"><?php echo $categoryInfo['category_name']?></td>
                        <td class="center"><?php echo $categoryInfo['category_description']?></td>
                         <td class="center">
                            <?php if ($categoryInfo['publication_status']==1){?>
                            <span class="label label-success"> <?php echo "Published"?>
                            </span> <?php }
                             else {?> <span class="label label-important"><?php echo "Unpublished"?></span><?php }?>
                         </td>   
                        
                        <td class="center">
                            
                            <?php if ($categoryInfo['publication_status']==1){?>
                            <a class="btn btn-danger" href="?publicationStatus=published&&categoryId=<?php echo $categoryInfo['category_id']?>" title="Unpublish">
                                <i class="halflings-icon white arrow-up"></i>  
                            </a>
                            <?php } else { ?> 
                             <a class="btn btn-success" href="?publicationStatus=unPublished&&categoryId=<?php echo $categoryInfo['category_id']?>" title="Publish">
                                <i class="halflings-icon white arrow-down"></i>  
                            </a>
                            <?php } ?>
                            <a class="btn btn-info" href="editCategory.php?categoryId=<?php echo $categoryInfo['category_id']?>" title="Edit">
                                <i class="halflings-icon white edit"></i>  
                            </a>
                            <a class="btn btn-danger" href="?publicationStatus=delete&&categoryId=<?php echo $categoryInfo['category_id']?>" title="Delete" onclick= "return  checkDelete()">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>
                    <?php }?>
                 
             
                </tbody>
            </table>            
        </div></div></div>
 
