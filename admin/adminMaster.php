<?php
ob_start();
session_start();
$adminId = isset($_SESSION['adminId']);

if ($adminId == null) {

    header('Location:index.php');
}
require '../classes/superAdmin.php';
$objSuperAdmin = new SuperAdmin();

if (isset($_GET['status'])) {
    if ($_GET['status'] == 'logOut') {

        $objSuperAdmin->logOut();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- start: Meta -->
        <meta charset="utf-8">
        <title>Eshopper || Admin</title>
        <meta name="description" content="Bootstrap Metro Dashboard">
        <meta name="author" content="Dennis Ji">
        <meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <!-- end: Meta -->

        <!-- start: Mobile Specific -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- end: Mobile Specific -->

        <!-- start: CSS -->
        <link id="bootstrap-style" href="../assets/adminAssets/css/bootstrap.min.css" rel="stylesheet">
        <link href="../assets/adminAssets/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link id="base-style" href="../assets/adminAssets/css/style.css" rel="stylesheet">
        <link id="base-style-responsive" href="../assets/adminAssets/css/style-responsive.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
        <!-- end: CSS -->


        <!-- start: Favicon -->
        <link rel="shortcut icon" href="../assets/adminAssets/img/favicon.ico">
        <!-- end: Favicon -->
        
        <script>
            function checkDelete(){
                var chk=confirm("Are you sure to delete this");
            
            if(chk){
                return true;
            }
            else {
                return false;
            }
        }
        </script>




    </head>

    <body>
        <!-- start: Header -->
<?php include '../admin/includes/adminHeader.php'; ?>
        <!-- start: Header -->

        <div class="container-fluid-full">
            <div class="row-fluid">

                <!-- start: Main Menu -->
                <?php include '../admin/includes/adminMenu.php'; ?>
                <!-- end: Main Menu -->

            </div>
            <div id="content" class="span10">
                <?php if(isset($pages)){
                        if ($pages=='adminHomeContent')include '../admin/pages/adminHomeContent.php';
                        elseif  ($pages=='adminCategoryContent')include '../admin/pages/addCategoryContent.php'; 
                        elseif  ($pages=='manageCategoryContent')include '../admin/pages/manageCategoryContent.php';
                        elseif  ($pages=='editCategoryContent')include '../admin/pages/editCategoryContent.php';
                        elseif  ($pages=='addManufacturerContent')include '../admin/pages/addManufacturerContent.php';
                        elseif  ($pages=='manageManufacturerContent')include '../admin/pages/manageManufacturerContent.php';
                        elseif  ($pages=='editManufacturerContent')include '../admin/pages/editManufacturerContent.php';
                        elseif  ($pages=='addProductContent')include '../admin/pages/addProductContent.php';   
                        elseif  ($pages=='manageProductContent')include '../admin/pages/manageProductContent.php';
                        elseif  ($pages=='editProductContent')include '../admin/pages/editProductContent.php';
                    }else {
                       include '../admin/pages/adminHomeContent.php'; 
                    }
                        
                      ?>
                
                
            </div>

                <!-- start: Content -->
<!--/#content.span10-->
        </div><!--/fluid-row-->
<div class="clearfix"></div>
   
                    

        

        <?php include '../admin/includes/adminFooter.php'; ?>

        <!-- start: JavaScript-->

        <script src="../assets/adminAssets/js/jquery-1.9.1.min.js"></script>
        <script src="../assets/adminAssets/js/jquery-migrate-1.0.0.min.js"></script>

        <script src="../assets/adminAssets/js/jquery-ui-1.10.0.custom.min.js"></script>

        <script src="../assets/adminAssets/js/jquery.ui.touch-punch.js"></script>

        <script src="../assets/adminAssets/js/modernizr.js"></script>

        <script src="../assets/adminAssets/js/bootstrap.min.js"></script>

        <script src="../assets/adminAssets/js/jquery.cookie.js"></script>

        <script src='../assets/adminAssets/js/fullcalendar.min.js'></script>

        <script src='../assets/adminAssets/js/jquery.dataTables.min.js'></script>

        <script src="../assets/adminAssets/js/excanvas.js"></script>
        <script src="../assets/adminAssets/js/jquery.flot.js"></script>
        <script src="../assets/adminAssets/js/jquery.flot.pie.js"></script>
        <script src="../assets/adminAssets/js/jquery.flot.stack.js"></script>
        <script src="../assets/adminAssets/js/jquery.flot.resize.min.js"></script>

        <script src="../assets/adminAssets/js/jquery.chosen.min.js"></script>

        <script src="../assets/adminAssets/js/jquery.uniform.min.js"></script>

        <script src="../assets/adminAssets/js/jquery.cleditor.min.js"></script>

        <script src="../assets/adminAssets/js/jquery.noty.js"></script>

        <script src="../assets/adminAssets/js/jquery.elfinder.min.js"></script>

        <script src="../assets/adminAssets/js/jquery.raty.min.js"></script>

        <script src="../assets/adminAssets/js/jquery.iphone.toggle.js"></script>

        <script src="../assets/adminAssets/js/jquery.uploadify-3.1.min.js"></script>

        <script src="../assets/adminAssets/js/jquery.gritter.min.js"></script>

        <script src="../assets/adminAssets/js/jquery.imagesloaded.js"></script>

        <script src="../assets/adminAssets/js/jquery.masonry.min.js"></script>

        <script src="../assets/adminAssets/js/jquery.knob.modified.js"></script>

        <script src="../assets/adminAssets/js/jquery.sparkline.min.js"></script>

        <script src="../assets/adminAssets/js/counter.js"></script>

        <script src="../assets/adminAssets/js/retina.js"></script>

        <script src="../assets/adminAssets/js/custom.js"></script>
        <!-- end: JavaScript-->

    </body>
</html>
