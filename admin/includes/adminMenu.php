<div id="sidebar-left" class="span2">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li><a href="adminHome.php"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>	
            <li><a href="adminCategory.php"><i class="icon-list"></i><span class="hidden-tablet">Add Category</span></a></li>
            <li><a href="manageCategory.php"><i class="icon-tasks"></i><span class="hidden-tablet">Manage Category</span></a></li>
            <li><a href="addManufacturer.php"><i class="icon-file-alt"></i><span class="hidden-tablet">Add Manufacturer</span></a></li>
            <li><a href="manageManufacturer.php"><i class="icon-tasks"></i><span class="hidden-tablet">Manage Manufacturer</span></a></li>
          
            <li><a href="addProduct.php"><i class="icon-list"></i><span class="hidden-tablet">Add Product</span></a></li>
            <li><a href="manageProduct.php"><i class="icon-list-alt"></i><span class="hidden-tablet">Manage Product</span></a></li>
            <li><a href=""><i class="icon-picture"></i><span class="hidden-tablet"> Gallery</span></a></li>
            <li><a href=""><i class="icon-align-justify"></i><span class="hidden-tablet"> Tables</span></a></li>
            <li><a href=""><i class="icon-calendar"></i><span class="hidden-tablet"> Calendar</span></a></li>
            <li><a href=""><i class="icon-folder-open"></i><span class="hidden-tablet"> File Manager</span></a></li>
            <li><a href=""><i class="icon-lock"></i><span class="hidden-tablet"> Login Page</span></a></li>
        </ul>
    </div>
</div>

